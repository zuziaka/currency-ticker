//
//  Double+Comparision.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import XCTest

class Double_Comparision: XCTestCase {
    
    func testComparision() {
        XCTAssertTrue(2.00 ~== 2.00)
        XCTAssertTrue(2.00 ~== 2.0000001)
        XCTAssertFalse(-2.00 ~== 2.00)
        XCTAssertTrue(2.001 ~== 2.00)
        XCTAssertFalse(2.01 ~== 2.00)
    }
}
