//
//  ExchangeRateTests.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import XCTest

class ExchangeRateTests: XCTestCase {
    
    func testInitialization() {
        let rate = ExchangeRate(factor: 2.0)
        XCTAssertTrue(rate.factor ~== 2.0)
        
        let testDate = Date().addingTimeInterval(20.0)
        let rateWithDate = ExchangeRate(factor: 2.0, date: testDate)
        XCTAssertTrue(rateWithDate.date == testDate)
        
        let floatRate: ExchangeRate = 2.0
        XCTAssertTrue(floatRate.factor ~== 2.0)
        
        let integerRate: ExchangeRate = 2
        XCTAssertTrue(integerRate.factor ~== 2.0)
    }
    
}
