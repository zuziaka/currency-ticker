//
//  Date+FormatTests.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import XCTest

class Date_FormatTests: XCTestCase {
    
    func testDateInitialization() {
        let correct_yMdDashFormat_string = "2016-12-10"
        let correct_yMdFormat_string = "2016/12/10"
        let correct_dMyFormat_string = "10/12/2016"
        let correct_yMdHmsFormat_string = "2016/12/10 10:12:12"
        let correct_yMdHmFormat_string = "2016/12/10 10:12"
        let correct_EdMFormat_string = "Thursday, 06/12"
        let correct_HmFormat_string = "12:12"
        let correct_dMFormat_string = "11/11"
        let correct_MdyFormat_string = "Oct 12, 1993"
        let correct_yMdTHmsZFormat_string = "2016/10/06T18:05:32Z"
        let correct_ISO8601_string = "2016-10-06T18:07:00+0200"
        let correct_EdMY_string = "Thursday 06/10/2016"
        let correct_yMd_string = "20151211"
        
        XCTAssertNotNil(Date(string: correct_yMdDashFormat_string, format: .yMdDashFormat))
        XCTAssertNotNil(Date(string: correct_yMdFormat_string, format: .yMdFormat))
        XCTAssertNotNil(Date(string: correct_dMyFormat_string, format: .dMyFormat))
        XCTAssertNotNil(Date(string: correct_yMdHmsFormat_string, format: .yMdHmsFormat))
        XCTAssertNotNil(Date(string: correct_yMdHmFormat_string, format: .yMdHmFormat))
        XCTAssertNotNil(Date(string: correct_EdMFormat_string, format: .EdMFormat))
        XCTAssertNotNil(Date(string: correct_HmFormat_string, format: .HmFormat))
        XCTAssertNotNil(Date(string: correct_dMFormat_string, format: .dMFormat))
        XCTAssertNotNil(Date(string: correct_MdyFormat_string, format: .MdyFormat))
        XCTAssertNotNil(Date(string: correct_yMdTHmsZFormat_string, format: .yMdTHmsZFormat))
        XCTAssertNotNil(Date(string: correct_ISO8601_string, format: .ISO8601))
        XCTAssertNotNil(Date(string: correct_EdMY_string, format: .EdMY))
        XCTAssertNotNil(Date(string: correct_yMd_string, format: .yMd))
    }
    
    func testDateFailureInitialization() {
        let incorrectDateString = "this is incorrect"
        
        XCTAssertNil(Date(string: incorrectDateString, format: .yMdDashFormat))
        XCTAssertNil(Date(string: incorrectDateString, format: .yMdFormat))
        XCTAssertNil(Date(string: incorrectDateString, format: .dMyFormat))
        XCTAssertNil(Date(string: incorrectDateString, format: .yMdHmsFormat))
        XCTAssertNil(Date(string: incorrectDateString, format: .yMdHmFormat))
        XCTAssertNil(Date(string: incorrectDateString, format: .EdMFormat))
        XCTAssertNil(Date(string: incorrectDateString, format: .HmFormat))
        XCTAssertNil(Date(string: incorrectDateString, format: .dMFormat))
        XCTAssertNil(Date(string: incorrectDateString, format: .MdyFormat))
        XCTAssertNil(Date(string: incorrectDateString, format: .yMdTHmsZFormat))
        XCTAssertNil(Date(string: incorrectDateString, format: .ISO8601))
        XCTAssertNil(Date(string: incorrectDateString, format: .EdMY))
        XCTAssertNil(Date(string: incorrectDateString, format: .yMd))
    }
    
    func testDateToStringConversion() {
        XCTAssertTrue(Date(string: "2016-12-10", format: .yMdDashFormat)?.toString(format: .yMdDashFormat) == "2016-12-10")
        XCTAssertTrue(Date(string: "2016/12/10", format: .yMdFormat)?.toString(format: .yMdFormat) == "2016/12/10")
        XCTAssertTrue(Date(string: "10/12/2016", format: .dMyFormat)?.toString(format: .dMyFormat) == "10/12/2016")
        XCTAssertTrue(Date(string: "2016/12/10 10:12:12", format: .yMdHmsFormat)?.toString(format: .yMdHmsFormat) == "2016/12/10 10:12:12")
        XCTAssertTrue(Date(string: "2016/12/10 10:12", format: .yMdHmFormat)?.toString(format: .yMdHmFormat) == "2016/12/10 10:12")
        XCTAssertTrue(Date(string: "12:12", format: .HmFormat)?.toString(format: .HmFormat) == "12:12")
        XCTAssertTrue(Date(string: "11/11", format: .dMFormat)?.toString(format: .dMFormat) == "11/11")
        XCTAssertTrue(Date(string: "Oct 12, 1993", format: .MdyFormat)?.toString(format: .MdyFormat) == "Oct 12, 1993")
        XCTAssertTrue(Date(string: "2016/10/06T18:05:32Z", format: .yMdTHmsZFormat)?.toString(format: .yMdTHmsZFormat) == "2016/10/06T18:05:32Z")
        XCTAssertTrue(Date(string: "2016-10-06T18:07:00+0200", format: .ISO8601)?.toString(format: .ISO8601) == "2016-10-06T18:07:00+0200")
        XCTAssertTrue(Date(string: "20151211", format: .yMd)?.toString(format: .yMd) == "20151211")
    }

    
}

    
