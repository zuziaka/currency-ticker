//
//  CurrencyTests.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import XCTest

class MoneyTests: XCTestCase {

    private let PLN = Currency(code: .PLN, exchangeRate: 1.0)
    private let EUR = Currency(code: .EUR, exchangeRate: 4.29)
    private var oneEuro: Money!
    private var onePLN: Money!
    
    override func setUp() {
        super.setUp()
        oneEuro = Money(currency: EUR, amount: 1.0)
        onePLN = Money(currency: PLN, amount: 1.0)
    }
    
    func testInitialization() {
        XCTAssertTrue(oneEuro.amount ~== 1.0)
        XCTAssertTrue(oneEuro.currency.factor ~== 4.29)
        XCTAssertTrue(onePLN.amount ~== 1.0)
        XCTAssertTrue(onePLN.currency.factor ~== 1.0)
    }
    
    func testConversion() {
        let onePLNinEUR = Money(currency: EUR, amount: 1.0/4.29)
        
        XCTAssertTrue(oneEuro.convert(to: PLN).amount == 4.29)
        XCTAssertFalse(oneEuro == onePLN)
        XCTAssertTrue(onePLN == onePLNinEUR)
    }
    
}
