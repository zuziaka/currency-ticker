//
//  UserManagerTests.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 12.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import XCTest

class UserManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        UserManager.removeAllFavorites()
    }
    
    override func tearDown() {
        super.tearDown()
        UserManager.removeAllFavorites()
    }
    
    func testAddingFavorites() {
        
        XCTAssertTrue(UserManager.favoriteCurrencyCodes.isEmpty)
        
        UserManager.add(favoriteCurrencyCode: .EUR)
        XCTAssertFalse(UserManager.favoriteCurrencyCodes.isEmpty)
        XCTAssertTrue(UserManager.favoriteCurrencyCodes.contains(.EUR))
    }
    
    func testAddingManyFavorites() {
        UserManager.add(favoriteCurrencyCode: .AUD)
        UserManager.add(favoriteCurrencyCode: .PLN)
        UserManager.add(favoriteCurrencyCode: .EUR)
        
        
        let codes = UserManager.favoriteCurrencyCodes
        XCTAssertTrue(codes.count == 3)
        XCTAssertTrue(codes.contains(.AUD))
        XCTAssertTrue(codes.contains(.PLN))
        XCTAssertTrue(codes.contains(.EUR))
    }

    func testRemovingAllFavorites() {
        UserManager.add(favoriteCurrencyCode: .AUD)
        UserManager.remove(favoriteCurrencyCode: .AUD)
        XCTAssertTrue(UserManager.favoriteCurrencyCodes.isEmpty)
        
        UserManager.add(favoriteCurrencyCode: .AUD)
        UserManager.add(favoriteCurrencyCode: .EUR)
        UserManager.add(favoriteCurrencyCode: .USD)
        
        UserManager.removeAllFavorites()
        XCTAssertTrue(UserManager.favoriteCurrencyCodes.isEmpty)
    }
    
    func testRemovingSingleFavorite() {
        UserManager.add(favoriteCurrencyCode: .PLN)
        UserManager.add(favoriteCurrencyCode: .USD)
        UserManager.add(favoriteCurrencyCode: .CHF)
        XCTAssertTrue(UserManager.favoriteCurrencyCodes.count == 3)
        
        UserManager.remove(favoriteCurrencyCode: .PLN)
        XCTAssertTrue(UserManager.favoriteCurrencyCodes.count == 2)
        XCTAssertFalse(UserManager.favoriteCurrencyCodes.contains(.PLN))
    }
    
    func testAddingSameCurrencyMultipleTimes() {
        UserManager.add(favoriteCurrencyCode: .PLN)
        UserManager.add(favoriteCurrencyCode: .PLN)
        UserManager.add(favoriteCurrencyCode: .PLN)
        
        XCTAssertTrue(UserManager.favoriteCurrencyCodes.count == 1)
    }
    
    func testIsFavoriteMethod() {
        UserManager.add(favoriteCurrencyCode: .PLN)
        XCTAssertTrue(UserManager.isFavorite(currencyCode: .PLN))
        
        UserManager.remove(favoriteCurrencyCode: .PLN)
        XCTAssertFalse(UserManager.isFavorite(currencyCode: .PLN))
    }
    
    //MARK: Base currency code

    func testAddingbaseCurrencyCode() {
        UserManager.add(baseCurrencyCode: .PLN)
        XCTAssertEqual(UserManager.baseCurrencyCode, .PLN)
    }
}
