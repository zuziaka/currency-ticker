//
//  HistoricalRatesResponseMapper.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import XCTest

class HistoricalRatesResponseMapperTests: XCTestCase {
    
    func testMappingEmptyData() {
        XCTAssertThrowsError(try HistoricalRatesResponseMapper.map(data: nil) as [CurrencyCode : [ExchangeRate]], "", { error in
            XCTAssertEqual(error as? ResponseMapperError, ResponseMapperError.invalid)
        })
    }
    
    func testMappingCorrectData() {
        let data = json(fromFile: "HistoricalRatesResponse_correct")
        do {
            let currenciesDictionary: [CurrencyCode : [ExchangeRate]] = try HistoricalRatesResponseMapper.map(data: data)
            
            let rates: [ExchangeRate]? = currenciesDictionary[.EUR]
            guard let euroRates = rates else {
                XCTFail()
                return
            }
            XCTAssertTrue(euroRates.count == 2)
            XCTAssertTrue(euroRates[0].factor ~== 4.191)
        } catch (let error) {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testMappingIncorrectData() {
        let data = json(fromFile: "HistoricalRatesResponse_missing_rates")
        XCTAssertThrowsError(try HistoricalRatesResponseMapper.map(data: data) as [CurrencyCode : [ExchangeRate]], "", { error in
            XCTAssertEqual(error as? ResponseMapperError, ResponseMapperError.missingAttribute)
        })
    }
}
