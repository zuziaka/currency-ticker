//
//  LatestRatesResponseMapperTests.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import XCTest
import Foundation

class LatestRatesResponseMapperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    func testMappingEmptyData() {
        XCTAssertThrowsError(try LatestRatesResponseMapper.map(data: nil) as [Currency], "", { error in
            XCTAssertEqual(error as? ResponseMapperError, ResponseMapperError.invalid)
        })
    }
    
    func testMappingCorrectData() {
        
        let data = json(fromFile: "LatestRatesResponse_correct")
        do {
            let currencies: [Currency] = try LatestRatesResponseMapper.map(data: data)
            
            let USD = currencies.filter { $0.code == .USD}[0]
            
            XCTAssertTrue(USD.factor ~== 3.8405)
            XCTAssertTrue(USD.exchangeRate.date.toString(format: .yMdDashFormat) == "2016-10-06")
        } catch (let error) {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testMappingIncorrectData() {
        let data = json(fromFile: "LatestRatesResponse_missing_rates")
        XCTAssertThrowsError(try LatestRatesResponseMapper.map(data: data) as [Currency], "", { error in
            XCTAssertEqual(error as? ResponseMapperError, ResponseMapperError.missingAttribute)
        })
    }
}

internal func json(fromFile file: String) -> Data {
    let bundle = Bundle(for: LatestRatesResponseMapperTests.self)
    let url = bundle.url(forResource: file, withExtension: "json")
    return try! Data(contentsOf: url!)
}

