//
//  ResponseMapperTests.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import XCTest

class ResponseMapperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    func testMappingEmptyData() {
        XCTAssertThrowsError(try ResponseMapper.map(data: nil), "", { error in
            XCTAssertEqual(error as? ResponseMapperError, ResponseMapperError.invalid)
        })
        let jsonData = try! JSONSerialization.data(withJSONObject: [:], options: .prettyPrinted)
        guard let _ = try? ResponseMapper.map(data: jsonData) else {
            XCTFail()
            return
        }
    }
    
    func testMappingCorrectData() {
        let jsonData = try! JSONSerialization.data(withJSONObject: ["Hello":"hello"], options: .prettyPrinted)
        guard let _ = try? ResponseMapper.map(data: jsonData) else {
            XCTFail()
            return
        }
    }
}
