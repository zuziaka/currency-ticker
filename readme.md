Develop: [![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=57f68e027280e101001c6bbc&branch=develop&build=latest)](https://dashboard.buddybuild.com/apps/57f68e027280e101001c6bbc/build/latest)
Master:
[![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=57f68e027280e101001c6bbc&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/57f68e027280e101001c6bbc/build/latest)
## Currency Ticker - demo application

### Core concepts
- *Dashboard* - User can see favorite currencies.
- *Currency Detail View* ­- shown after tapping on currency in dashboard. It should display currency chart and some more details.
- *Favorites Manager* -­ user can add/edit/remove favorite currency tickers. Default should
be currency based on current locale (editable).

### Additional features
- 3D Touch on cells

### Supported Devices
- iPhone 4s+
- iPad
- both landscape and portret mode

### Architecture
Application is based on Clean Swift (aka VIPER).
### How to build?
- Install Cocoapods (if needed) `1.0.1`
- Run `pod install` in project directory
- Build with Xcode 8 + Swift 3.0

### Dependencies
- Moya (7.0.0) - network layer
- SwiftCharts (0.4) - as iOS-Charts does not support Swift 3.0
- Pure Layout (3.0.0)- AutoLayout wrapper
