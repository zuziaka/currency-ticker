//
//  HistoricalRatesResponseMapper.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

final class HistoricalRatesResponseMapper: ResponseMapper {
    typealias Item = [CurrencyCode: [ExchangeRate]]
    
    static func map(data: Data?) throws -> [CurrencyCode: [ExchangeRate]] {
        guard let data = try? super.map(data: data),
        let json = data as? [[String: Any]]
        else {
            throw ResponseMapperError.invalid
        }
        
        var resultData: [CurrencyCode: [ExchangeRate]] = [:]
        for case let table: [String: Any] in json {
            guard let dateString = table["effectiveDate"] as? String,
                let date = Date(string: dateString, format: .yMdDashFormat),
                let rates = table["rates"] as? [[String: Any]]
                else {
                    throw ResponseMapperError.missingAttribute
            }
            if resultData[.PLN] == nil {
                resultData[.PLN] = [ExchangeRate(factor: 1.0, date: date)]
            } else {
                resultData[.PLN]?.append(ExchangeRate(factor: 1.0, date: date))
            }
            let currencies: [Currency] = rates.flatMap { rate in
                guard let factor = rate["mid"] as? Double,
                    let code = rate["code"] as? String,
                    let currencyCode = CurrencyCode(rawValue: code)
                    else { return nil }
                let exchangeRate = ExchangeRate(factor: factor, date: date)
                return Currency(code: currencyCode, exchangeRate: exchangeRate)
            }
            
            for currency in currencies {
                if resultData[currency.code] != nil {
                    resultData[currency.code]!.append(currency.exchangeRate)
                } else {
                    resultData[currency.code] = [currency.exchangeRate]
                }
            }
        }
        return resultData
    }
}
