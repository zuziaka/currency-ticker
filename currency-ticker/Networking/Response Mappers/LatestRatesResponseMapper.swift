//
//  LatestRatesResponseMapper.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

final class LatestRatesResponseMapper: ResponseMapper {
    typealias Item = [Currency]
    
    static func map(data: Data?) throws -> [Currency] {
        guard let data = try? super.map(data: data),
        let mainArray = data as? [Any],
        let json = mainArray[0] as? [String: Any]
        else {
            throw ResponseMapperError.invalid
        }
        
        guard let dateString = json["effectiveDate"] as? String,
            let date = Date(string: dateString, format: .yMdDashFormat),
            let rates = json["rates"] as? [[String: Any]]
            else {
                throw ResponseMapperError.missingAttribute
        }
        
        var currencies: [Currency] = rates.flatMap { rate in
            guard let factor = rate["mid"] as? Double,
                let code = rate["code"] as? String,
                let currencyCode = CurrencyCode(rawValue: code)
                else { return nil }
            let exchangeRate = ExchangeRate(factor: factor, date: date)
            return Currency(code: currencyCode, exchangeRate: exchangeRate)
        }
        currencies.append(Currency(code: .PLN, exchangeRate: 1.0))
        return currencies
    }
}
