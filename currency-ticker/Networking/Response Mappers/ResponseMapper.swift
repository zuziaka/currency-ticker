//
//  ResponseMapper.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

internal enum ResponseMapperError: Error {
    case invalid
    case missingAttribute
}

internal protocol ResponseMapperProtocol {
    associatedtype Item
    static func map(data: Data?) throws -> Item
}

class ResponseMapper: ResponseMapperProtocol {
    typealias Item = Any
    
    static func map(data: Data?) throws -> Item {
        guard let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
            throw ResponseMapperError.invalid
        }
        return json
    }
}

