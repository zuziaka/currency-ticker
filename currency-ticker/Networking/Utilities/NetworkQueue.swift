//
//  NetworkQueue.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

public class NetworkQueue {
    
    public static var shared: NetworkQueue = NetworkQueue()
    
    let queue = OperationQueue()
    
    public init() {}
    
    public func add(operation: Operation) {
        queue.addOperation(operation)
    }
}
