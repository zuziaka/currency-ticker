//
//  GetHistoricalRatesOperation.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 11.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Moya
import Result

enum GetHistoricalRatesOperationError: Swift.Error {
    case generic
    case internetConnection
}

class GetHistoricalRatesOperation: ConcurrentOperation {
    public typealias GetHistoricalRatesOperationCompletion = (Result<[CurrencyCode: [ExchangeRate]], GetHistoricalRatesOperationError>) -> Void
    
    let completion: GetHistoricalRatesOperationCompletion
    let startDate: Date
    let endDate: Date
    
    init(startDate: Date, endDate: Date, completion: @escaping GetHistoricalRatesOperationCompletion) {
        self.completion = completion
        self.startDate = startDate
        self.endDate = endDate
    }
    
    override func main() {
        super.main()
        let provider = MoyaProvider<ExchangeRatesProvider>(plugins: [NetworkLogger()])
        let target = ExchangeRatesProvider(request: .historical(startDate: startDate, endDate: endDate), credentials: ExchangeRatesProviderCredentials(baseURL: URL(string: "http://api.nbp.pl/api")!))
        provider.request(target, completion: { responseResult in
            switch ResponseController.handle(result: responseResult) {
            case .success(let data):
                if let rates = try? HistoricalRatesResponseMapper.map(data: data) {
                    self.finish(withRates: rates)
                } else {
                    self.finish(withError: .generic)
                }
            case .failure(let error):
                switch error {
                case .timeout:
                    self.finish(withError: .internetConnection)
                default:
                    self.finish(withError: .generic)
                }
            }
        })
    }
    
    private func finish(withRates rates: [CurrencyCode: [ExchangeRate]]) {
        completion(.success(rates))
        finish()
    }
    
    private func finish(withError error: GetHistoricalRatesOperationError) {
        completion(.failure(error))
        finish()
    }
}
