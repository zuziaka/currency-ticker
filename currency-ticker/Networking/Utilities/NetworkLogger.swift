//
//  NetworkLogger.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

import Foundation
import Moya
import Result

public struct NetworkLogger: PluginType {
    public init() {}
    public func willSendRequest(_ request: RequestType, target: TargetType) {
        print("______")
        print("Sending request: \(request.request?.httpMethod ?? ""): \(request.request?.url?.absoluteString ?? String())")
        print("Headers: \(request.request?.allHTTPHeaderFields ?? [:])")
        print("______")
    }
    
    public func didReceiveResponse(_ result: Result<Moya.Response, Moya.Error>, target: TargetType) {
        print("______")
        switch result {
        case .success(let response):
            print("Received Response: \(response.statusCode) ")
        case .failure(let error):
            print("Received networking error: \(error)")
        }
        print("______")
    }
}
