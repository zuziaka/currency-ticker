//
//  ResponseController.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Moya
import Result

public enum NetworkError {
    case unauthorized
    case generic
    case notFound
    case incorrectData
    case timeout
}

extension NetworkError: Swift.Error { }

public class ResponseController {
    
    public init() {}
    
    static func handle(statusCode: Int) -> NetworkError? {
        switch statusCode {
        case 200...300:
            return nil
        case 401:
            return NetworkError.unauthorized
        case 404:
            return NetworkError.notFound
        default:
            return NetworkError.generic
        }
    }
}

extension ResponseController {
    
    static public func handle(response: Moya.Response?) -> Result<Data, NetworkError> {
        if let response = response {
            if let error = ResponseController.handle(statusCode: response.statusCode) {
                return .failure(error)
            } else {
                return .success(response.data)
            }
        } else {
            return .failure(NetworkError.generic)
        }
    }
    
    static public func handle(result: Result<Moya.Response, Moya.Error>) -> Result<Data, NetworkError> {
        switch result {
        case .success(let response):
            return ResponseController.handle(response: response)
        case .failure(_):
            return .failure(.timeout)
        }
    }
}
