//
//  GetLatestRatesOperation.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Result
import Moya

enum GetLatestRatesOperationError: Swift.Error {
    case generic
    case internetConnection
}

class GetLatestRatesOperation: ConcurrentOperation {
    public typealias GetLatestRatesOperationCompletion = (Result<[Currency], GetLatestRatesOperationError>) -> Void
    
    let completion: GetLatestRatesOperationCompletion
    
    init(completion: @escaping GetLatestRatesOperationCompletion) {
        self.completion = completion
    }
    
    override func main() {
        super.main()
        let provider = MoyaProvider<ExchangeRatesProvider>(plugins: [NetworkLogger()])
        let target = ExchangeRatesProvider(request: .latest, credentials: ExchangeRatesProviderCredentials(baseURL: URL(string: "http://api.nbp.pl/api")!))
        provider.request(target, completion: { responseResult in
            switch ResponseController.handle(result: responseResult) {
            case .success(let data):
                if let currencies = try? LatestRatesResponseMapper.map(data: data) {
                    self.finish(withCurrencies: currencies)
                } else {
                    self.finish(withError: .generic)
                }
            case .failure(let error):
                switch error {
                case .timeout:
                    self.finish(withError: .internetConnection)
                default:
                    self.finish(withError: .generic)
                }
            }
        })
    }
    
    private func finish(withCurrencies currencies: [Currency]) {
        completion(.success(currencies))
        finish()
    }
    
    private func finish(withError error: GetLatestRatesOperationError) {
        completion(.failure(error))
        finish()
    }
}
