//
//  ExchangeRatesManager.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Moya

struct ExchangeRatesProviderCredentials {
    let baseURL: URL
}

struct ExchangeRatesProvider {
    let credentials: ExchangeRatesProviderCredentials
    let request: Request
    
    init(request: Request, credentials: ExchangeRatesProviderCredentials) {
        self.request = request
        self.credentials = credentials
    }
    
    enum Request {
        case latest
        case historical(startDate: Date, endDate: Date)
    }
}

extension ExchangeRatesProvider: TargetType {
    var baseURL: URL { return credentials.baseURL }
    var path: String {
        switch request {
        case .latest:
            return "/exchangerates/tables/A/"
        case .historical(let startDate, let endDate):
            return "/exchangerates/tables/A/\(startDate.toString(format: .yMdDashFormat))/\(endDate.toString(format: .yMdDashFormat))"
        }
    }
    var method: Moya.Method { return .GET }
    var parameters: [String: Any]? { return nil }
    var sampleData: Data { return Data() }
    var task: Task { return .request }
}
