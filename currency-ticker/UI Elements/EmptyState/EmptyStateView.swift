//
//  EmptyStateView.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

public class EmptyStateView: UIView {

    var contentView : UIView?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var onTap: ((Void) -> Void)?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        contentView = loadViewFromNib()
        contentView!.frame = bounds
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(contentView!)
    }
    
    private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "EmptyStateView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    public func setup(withUserInterface userInterface: UserInterface) {
        titleLabel.font = userInterface.font.regular(withSize: 15.0)
        actionButton.titleLabel?.font = userInterface.font.bold(withSize: 12.0)
        titleLabel.textColor = userInterface.color.plain
        actionButton.setTitleColor(userInterface.color.tint, for: .normal)
        backgroundColor = userInterface.color.background
        contentView?.backgroundColor = userInterface.color.background
    }
    
    @IBAction func actionButtonTapped(_ sender: AnyObject) {
        if let onTap = onTap {
            onTap()
        }
    }
    
    public func hide() {
        onTap = nil
        set(hidden: true)
    }
    
    public func show() {
        set(hidden: false)
    }
    
    private func set(hidden: Bool) {
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = hidden ? 0.0 : 1.0
        }, completion: { completed in
            if completed {
                self.isHidden = hidden
                self.alpha = 1.0
            }
        })
    }
    
    public func showLoadingState(title: String) {
        show()
        onTap = nil
        showTitleLabel(with: title)
        actionButton.isHidden = true
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }
    
    public func showEmptyState(title: String, actionButtonTitle: String? = nil, onTap: ((Void) -> Void)? = nil) {
        show()
        showTitleLabel(with: title)
        if let buttonTitle = actionButtonTitle, let onTap = onTap {
            actionButton.setTitle(buttonTitle, for: .normal)
            actionButton.isHidden = false
            self.onTap = onTap
        } else {
            self.onTap = nil
            actionButton.isHidden = true
        }
        activityIndicator.isHidden = true
    }
    
    private func showTitleLabel(with title: String) {
        titleLabel.text = title
        titleLabel.isHidden = false
    }

}
