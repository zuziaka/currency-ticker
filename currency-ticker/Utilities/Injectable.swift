//
//  Injectable.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 09.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import UIKit


public protocol StoryboardInjectable {
    associatedtype T
    static func create(with: T) -> Self
}
