//
//  Date+Format.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation


fileprivate extension DateFormatter {
    fileprivate static func create(withFormat format: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter
    }
}

public enum DateFormat: String {
    case yMdDashFormat = "yyyy-MM-dd"
    case yMdFormat = "yyyy/MM/dd"
    case dMyFormat = "dd/MM/yyyy"
    case yMdHmsFormat = "yyyy/MM/dd HH:mm:ss"
    case yMdHmFormat = "yyyy/MM/dd HH:mm"
    case EdMFormat = "EEEE, dd/MM"
    case HmFormat = "HH:mm"
    case dMFormat = "dd/MM"
    case MdyFormat = "MMM dd, yyyy"
    case yMdTHmsZFormat = "yyyy/MM/dd'T'HH:mm:ss'Z'"
    case ISO8601 = "yyyy-MM-dd'T'HH:mm:ssZ"
    case EdMY = "EEEE dd/MM/YYYY"
    case yMd = "yyyyMMdd"
}

public extension Date {
    
    public func toString(format: DateFormat = .yMdHmsFormat) -> String {
        let formatter = DateFormatter.create(withFormat: format.rawValue)
        return formatter.string(from: self)
    }
    
    public init?(string: String, format: DateFormat = .yMdHmsFormat) {
        guard let date = DateFormatter.create(withFormat: format.rawValue).date(from: string) else {
            return nil
        }
        self = Date(timeInterval: 0, since: date)
    }
}
