//
//  String+Localized.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

public extension String {
    
    public func localized(bundle: Bundle = Bundle.main) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
    }
}
