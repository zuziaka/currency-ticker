//
//  CurrenciesConfigurator.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

// MARK: Connect View, Interactor, and Presenter

extension CurrenciesViewController: CurrenciesPresenterOutput { }

extension CurrenciesInteractor: CurrenciesViewControllerOutput { }

extension CurrenciesPresenter: CurrenciesInteractorOutput { }

class CurrenciesConfigurator {
    // MARK: Object lifecycle

    static let sharedInstance = CurrenciesConfigurator()
    private init() { }

    // MARK: Configuration

    func configure(viewController: CurrenciesViewController) {
        let router = CurrenciesRouter()
        router.viewController = viewController

        let presenter = CurrenciesPresenter()
        presenter.output = viewController

        let interactor = CurrenciesInteractor()
        interactor.output = presenter

        viewController.output = interactor
        viewController.router = router
        viewController.userInterface = ApplicationUserInterface()
    }
}
