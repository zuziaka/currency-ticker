//
//  CurrenciesPresenter.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

protocol CurrenciesPresenterInput {
    func present(currencyCodes: [CurrencyCode])
}

protocol CurrenciesPresenterOutput: class {
    func display(currenciesViewModel: [CurrenciesViewModel.Currencies])
}

class CurrenciesPresenter: CurrenciesPresenterInput {
    weak var output: CurrenciesPresenterOutput!

    func present(currencyCodes: [CurrencyCode]) {
        output.display(currenciesViewModel: currencyCodes.map { currencyCode in
            let currency = Currency(code: currencyCode, exchangeRate: 1.0)
            return CurrenciesViewModel.Currencies(title: currencyCode.rawValue, subtitle: currency.name.capitalized)
        })
    }

}
