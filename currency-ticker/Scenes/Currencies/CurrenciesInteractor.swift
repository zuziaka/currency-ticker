//
//  CurrenciesInteractor.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

protocol CurrenciesInteractorInput {
    func loadInitialState()
    var currencyCodes: [CurrencyCode] { get }
}

protocol CurrenciesInteractorOutput {
    func present(currencyCodes: [CurrencyCode])
}

class CurrenciesInteractor: CurrenciesInteractorInput {
    var output: CurrenciesInteractorOutput!
    var currencyCodes: [CurrencyCode] {
        return CurrencyCode.allValues.sorted(by: {$1.rawValue > $0.rawValue }).filter({return $0 != UserManager.baseCurrencyCode })
    }
    
    func loadInitialState() {
        output.present(currencyCodes: currencyCodes)
    }
}
