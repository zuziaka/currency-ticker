//
//  CurrenciesRouter.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

protocol CurrenciesRouterInput {
    func navigate(toCurrencyDetailViewController code: CurrencyCode)
    func viewController(forCurrency: CurrencyCode) -> UIViewController
    func navigate(toViewController: UIViewController)
}

class CurrenciesRouter: CurrenciesRouterInput {
    weak var viewController: CurrenciesViewController!

    func navigate(toCurrencyDetailViewController code: CurrencyCode) {
        let currencyDetailViewController = viewController(forCurrency: code)
        navigate(toViewController: currencyDetailViewController)
    }
    
    func viewController(forCurrency code: CurrencyCode) -> UIViewController {
        return CurrencyDetailViewController.create(with: code)
    }
    
    func navigate(toViewController viewController: UIViewController) {
        if let vc = self.viewController, let navigationController = vc.navigationController {
            navigationController.pushViewController(viewController, animated: true)
        }
    }
}
