//
//  CurrenciesModels.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

struct CurrenciesViewModel {
    struct Currencies {
        let title: String
        let subtitle: String
    }
}
