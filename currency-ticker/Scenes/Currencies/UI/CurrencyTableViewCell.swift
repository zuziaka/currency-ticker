//
//  CurrencyTableViewCell.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 12.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit


class CurrencyTableViewCell: UITableViewCell {
    static let identifier: String = "CurrencyTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    func configure(title: String, subtitle: String, userInerface: UserInterface) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
        configure(userInterface: userInerface)
    }
    
    private func configure(userInterface: UserInterface) {
        titleLabel.font = userInterface.font.bold(withSize: 17.0)
        subtitleLabel.font = userInterface.font.regular(withSize: 15.0)
    }
}
