//
//  SettingsPresenter.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 12.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

protocol SettingsPresenterInput {
    func present(currencyCodes: [CurrencyCode])
    func present(selectedCurrencyCode: CurrencyCode)
}

protocol SettingsPresenterOutput: class {
    func display(pickerViewCurrencies: [SettingsViewModel.CurrencyCode])
    func display(selectedCurrencyCode: String)
}

class SettingsPresenter: SettingsPresenterInput {
    weak var output: SettingsPresenterOutput!

    // MARK: Presentation logic

    func present(currencyCodes: [CurrencyCode]) {
        output.display(pickerViewCurrencies: currencyCodes.map { SettingsViewModel.CurrencyCode(title: "\($0.rawValue)") } )
    }
    
    func present(selectedCurrencyCode: CurrencyCode) {
        output.display(selectedCurrencyCode: selectedCurrencyCode.rawValue)
    }
}
