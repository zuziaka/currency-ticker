//
//  SettingsConfigurator.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 12.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

// MARK: Connect View, Interactor, and Presenter

extension SettingsViewController: SettingsPresenterOutput { }

extension SettingsInteractor: SettingsViewControllerOutput { }

extension SettingsPresenter: SettingsInteractorOutput { }

class SettingsConfigurator {

    static let sharedInstance = SettingsConfigurator()
    private init() { }

    func configure(viewController: SettingsViewController) {

        let presenter = SettingsPresenter()
        presenter.output = viewController

        let interactor = SettingsInteractor()
        interactor.output = presenter

        viewController.output = interactor
        viewController.userInterface = ApplicationUserInterface()
    }
}
