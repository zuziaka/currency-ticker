//
//  SettingsModels.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 12.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

struct SettingsViewModel {
    struct CurrencyCode {
        let title: String
    }
}
