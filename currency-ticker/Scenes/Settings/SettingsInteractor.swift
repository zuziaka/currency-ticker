//
//  SettingsInteractor.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 12.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

protocol SettingsInteractorInput {
    func loadInitialState()
    func didSelectCurrency(atIndex: Int)
    var currencyCodes: [CurrencyCode] { get }
}

protocol SettingsInteractorOutput {
    func present(currencyCodes: [CurrencyCode])
    func present(selectedCurrencyCode: CurrencyCode)
}

class SettingsInteractor: SettingsInteractorInput {
    var output: SettingsInteractorOutput!
    var currencyCodes: [CurrencyCode] = CurrencyCode.allValues.sorted(by: {$1.rawValue > $0.rawValue })
    
    func loadInitialState() {
        output.present(currencyCodes: currencyCodes)
        output.present(selectedCurrencyCode: UserManager.baseCurrencyCode)
    }
    
    func didSelectCurrency(atIndex index: Int) {
        let selectedCode = currencyCodes[index]
        UserManager.add(baseCurrencyCode: selectedCode)
        output.present(selectedCurrencyCode: selectedCode)
    }
}
