//
//  DashboardModels.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

struct DashboardViewModel {
    struct EmptyState {
        let title: String
        let actionTitle: String
    }
    
    struct Currencies {
        let currencies: [Currency]
        var count: Int { return currencies.count }
        
        init() { self.currencies = [] }
        init(currencies: [Currency]) { self.currencies = currencies }
        
        struct Currency {
            let title: String
            let subtitle: String
        }
    }
    
    struct LoadingState {
        let title: String
    }
}
