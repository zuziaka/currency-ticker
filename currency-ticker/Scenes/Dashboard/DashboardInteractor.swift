//
//  DashboardInteractor.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Result

protocol DashboardInteractorInput {
    func loadData()
    var currencies: [Currency] { get }
    var baseCurrency: Currency! { get }
}

protocol DashboardInteractorOutput {
    func presentLoadingState()
    func present(error: GetLatestRatesOperationError)
    func present(currencies: [Currency], baseCurrency: Currency)
    func presentNoFavoritesState()
}

class DashboardInteractor: DashboardInteractorInput {
    var output: DashboardInteractorOutput!
    var worker: DashboardWorker = DashboardWorker()
    var currencies: [Currency] = []
    var baseCurrency: Currency!
    
    func loadData() {
        output.presentLoadingState()
        
        guard UserManager.favoriteCurrencyCodes.isEmpty == false else {
            output.presentNoFavoritesState()
            return
        
        }
        worker.loadFavoriteCurrencies(completion: { [weak self] response in
            self?.handle(response)
        })
    }
    
    private func handle(_ response: Result<(Currency, [Currency]), GetLatestRatesOperationError>) {
        switch response {
        case .success(let favoriteCurrenciesResult):
            let baseCurrency = favoriteCurrenciesResult.0
            let currencies = favoriteCurrenciesResult.1
            self.currencies = currencies
            self.baseCurrency = baseCurrency
            
            output.present(currencies: currencies, baseCurrency: baseCurrency)
        case .failure(let error):
            currencies = []
            output.present(error: error)
        }
    }
    
}
