//
//  DashboardConfigurator.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

// MARK: Connect View, Interactor, and Presenter

extension DashboardViewController: DashboardPresenterOutput { }

extension DashboardInteractor: DashboardViewControllerOutput { }

extension DashboardPresenter: DashboardInteractorOutput { }

class DashboardConfigurator {
    // MARK: Object lifecycle

    static let sharedInstance = DashboardConfigurator()
    private init() { }

    // MARK: Configuration

    func configure(viewController: DashboardViewController) {
        let router = DashboardRouter()
        router.viewController = viewController

        let presenter = DashboardPresenter()
        presenter.output = viewController

        let interactor = DashboardInteractor()
        interactor.output = presenter

        viewController.output = interactor
        viewController.router = router
        viewController.userInterface = ApplicationUserInterface()
    }
}
