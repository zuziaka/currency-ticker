//
//  DashboardPresenter.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Result

protocol DashboardPresenterInput {
    func presentLoadingState()
    func present(error: GetLatestRatesOperationError)
    func present(currencies: [Currency], baseCurrency: Currency)
    func presentNoFavoritesState()
}

protocol DashboardPresenterOutput: class {
    func display(emptyState: DashboardViewModel.EmptyState)
    func display(loadingState: DashboardViewModel.LoadingState)
    func display(viewModel: DashboardViewModel.Currencies)
    func display(error: DashboardViewModel.EmptyState)
}

class DashboardPresenter: DashboardPresenterInput {
    weak var output: DashboardPresenterOutput!
    
    func presentLoadingState() {
        output.display(loadingState: DashboardViewModel.LoadingState(title: "loading".localized()))
    }
    
    func present(error: GetLatestRatesOperationError) {
        switch error {
        case .internetConnection:
            output.display(error: DashboardViewModel.EmptyState(title: "internet_connection_error".localized(), actionTitle: "reload".localized()))
        default:
            output.display(error: DashboardViewModel.EmptyState(title: "generic_error".localized(), actionTitle: "reload".localized()))
        }
    }
    
    func present(currencies: [Currency], baseCurrency: Currency) {
        if currencies.isEmpty {
            presentNoFavoritesState()
        } else {
            let viewModel = DashboardViewModel.Currencies(currencies: currencies.map { currency in
                let amount = Money(currency: baseCurrency, amount: 1.0).convert(to: currency).amount
                let f = NumberFormatter()
                f.maximumFractionDigits = 4
                return DashboardViewModel.Currencies.Currency(title: f.string(from: NSNumber(value: amount))!, subtitle: currency.code.rawValue)
            })
            output.display(viewModel: viewModel)
        }
    }
    
    func presentNoFavoritesState() {
        let emptyState = DashboardViewModel.EmptyState(title: "no_favorite_currencies".localized(), actionTitle: "add_favorite_currencies".localized())
        output.display(emptyState: emptyState)
    }
}
