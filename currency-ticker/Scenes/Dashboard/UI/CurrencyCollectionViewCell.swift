//
//  CurrencyCollectionViewCell.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 12.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import UIKit

class CurrencyCollectionViewCell: UICollectionViewCell {
    static let identifier = "CurrencyCollectionViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    func configure(title: String, subtitle: String, interface: UserInterface) {
        configure(userInterface: interface)
        addShadow()
        
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
    
    private func configure(userInterface interface: UserInterface) {
        titleLabel.font = interface.font.bold(withSize: 16.0)
        subtitleLabel.font = interface.font.regular(withSize: 12.0)
        
        titleLabel.textColor = interface.color.tint
        subtitleLabel.textColor = interface.color.plain
    }
 
    private func addShadow() {
        self.layer.cornerRadius = 5.0
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        layer.shadowRadius = 20.0
        layer.shadowOpacity = 1.0
    }
    
    override var isSelected: Bool {
        didSet {
            animateTouchEvent(touched: isSelected)
        }
    }
    
    override var isHighlighted: Bool {
        didSet { animateTouchEvent(touched: isHighlighted) }
    }
    
    private func animateTouchEvent(touched: Bool) {
        UIView.animate(withDuration: 0.4, animations: {
            if touched {
                self.alpha = 0.5
            } else {
                self.alpha = 1.0
            }
        })
    }
}
