//
//  DashboardWorker.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Result


class DashboardWorker {

    func loadFavoriteCurrencies(completion: @escaping (Result<(Currency, [Currency]), GetLatestRatesOperationError>) -> Void) {
        let operation = GetLatestRatesOperation(completion: { result in
            switch result {
            case .success(let currencies):
                completion(.success(self.favoriteCurrencies(fromCurrencies: currencies)))
            case .failure(let error):
                completion(.failure(error))
            }
        })
        NetworkQueue.shared.add(operation: operation)
    }
    
    private func favoriteCurrencies(fromCurrencies currencies: [Currency]) -> (Currency, [Currency]) {
        let favoriteCurrencyCodes = UserManager.favoriteCurrencyCodes
        let favoriteCurrencies = currencies.filter {
            return favoriteCurrencyCodes.contains($0.code)
        }
        let baseCurrencyResult = currencies.filter { $0.code == UserManager.baseCurrencyCode }
        return (baseCurrencyResult[0], favoriteCurrencies)
    }
}
