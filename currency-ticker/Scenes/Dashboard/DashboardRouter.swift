//
//  DashboardRouter.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

protocol DashboardRouterInput {
    func navigateToCurrenciesViewController()
    func navigate(toCurrencyDetailViewController currency: Currency, baseCurrency: Currency)
    func viewController(forCurrency: Currency, baseCurrency: Currency) -> UIViewController
    func navigate(toViewController: UIViewController)
}

class DashboardRouter: DashboardRouterInput {
    weak var viewController: DashboardViewController!

    func navigateToCurrenciesViewController() {
        if let tabbar = viewController.tabBarController {
            tabbar.selectedIndex = 1
        }
    }
    
    
    func navigate(toCurrencyDetailViewController currency: Currency, baseCurrency: Currency) {
        let currencyDetailViewController = viewController(forCurrency: currency, baseCurrency: baseCurrency)
        navigate(toViewController: currencyDetailViewController)
    }
    
    func viewController(forCurrency currency: Currency, baseCurrency: Currency) -> UIViewController {
        return CurrencyDetailViewController.create(with: currency, baseCurrency: baseCurrency)
    }
    
    func navigate(toViewController viewController: UIViewController) {
        if let vc = self.viewController, let navigationController = vc.navigationController {
            navigationController.pushViewController(viewController, animated: true)
        }
    }
}
