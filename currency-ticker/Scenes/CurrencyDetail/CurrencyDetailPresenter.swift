//
//  CurrencyDetailPresenter.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 09.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Result

protocol CurrencyDetailPresenterInput {
    func present(currentRateResponse: Result<CurrencyDetailResponse.CurrentRate, GetLatestRatesOperationError>)
    func present(currency: Currency, baseCurrency: Currency)
    func present(historicalRatesResponse response: Result<CurrencyDetailResponse.HistoricalRates, GetHistoricalRatesOperationError>)
    func presentMainLoadingState()
    func presentChartLoadingState()
    func presentFavoritesButton(selected: Bool)
}

protocol CurrencyDetailPresenterOutput: class {
    func display(viewModel: CurrencyDetailViewModel.CurrencyInfo)
    func display(viewModel: CurrencyDetailViewModel.CurrencyHistory)
    func display(mainError: CurrencyDetailViewModel.Error)
    func display(chartError: CurrencyDetailViewModel.Error)
    func display(mainLoadingState: CurrencyDetailViewModel.LoadingState)
    func display(chartLoadingState: CurrencyDetailViewModel.LoadingState)
    func display(favoritesButton: CurrencyDetailViewModel.FavoritesButton)
}

class CurrencyDetailPresenter: CurrencyDetailPresenterInput {
    weak var output: CurrencyDetailPresenterOutput!

    func present(currentRateResponse response: Result<CurrencyDetailResponse.CurrentRate, GetLatestRatesOperationError>) {
        switch response {
        case .success(let result):
            present(currency: result.currency, baseCurrency: result.baseCurrency)
        case .failure(let error):
            switch error {
            case .internetConnection:
                output.display(mainError: CurrencyDetailViewModel.Error(description: "internet_connection_error".localized(), actionTitle: "reload".localized()))
            default:
                output.display(mainError: CurrencyDetailViewModel.Error(description: "generic_error".localized(), actionTitle: "reload".localized()))
            }
        }
    }
    
    func present(currency: Currency, baseCurrency: Currency) {
        let baseMoney = Money(currency: baseCurrency, amount: 1.0)
        let money = baseMoney.convert(to: currency)
        let viewModel = CurrencyDetailViewModel.CurrencyInfo(exchangeRate: "\(baseMoney.description) = \(money.description)",
            base: baseCurrency.code.rawValue,
            title: currency.code.rawValue,
            description: "\(currency.code.rawValue)_description".localized(),
            date: currency.exchangeRate.date.toString(format: .yMdFormat),
            source: "NBP")
        
        output.display(viewModel: viewModel)
    }
    
    func present(historicalRatesResponse response: Result<CurrencyDetailResponse.HistoricalRates, GetHistoricalRatesOperationError>) {
        switch response {
        case .success(let result):
            let baseCurrencies = result.baseCurrencies.sorted(by: { $0.exchangeRate.date < $1.exchangeRate.date })
            let currencies = result.currencies.sorted(by: { $0.exchangeRate.date < $1.exchangeRate.date })
            var formatter: NumberFormatter?
            
            let models: [(Double, String)] = zip(baseCurrencies, currencies).map {
                let money = Money(currency: $0.0, amount: 1.0).convert(to: $0.1)
                formatter = money.numberFormatter
                return (money.amount, $0.0.exchangeRate.date.toString(format: .dMFormat))
            }

            let viewModel = CurrencyDetailViewModel.CurrencyHistory(rates: models.map { $0.0 }, formatter: formatter, labels: models.map { $0.1 })
            
            output.display(viewModel: viewModel)
        case .failure(let error):
            switch error {
            case .internetConnection:
                output.display(chartError: CurrencyDetailViewModel.Error(description: "internet_connection_error".localized(), actionTitle: "reload".localized()))
            default:
                output.display(chartError: CurrencyDetailViewModel.Error(description: "generic_error".localized(), actionTitle: "reload".localized()))
            }
            
        }
    }
    
    func presentMainLoadingState() {
        output.display(mainLoadingState: CurrencyDetailViewModel.LoadingState(description: "loading".localized()))
    }
    
    func presentChartLoadingState() {
        output.display(chartLoadingState: CurrencyDetailViewModel.LoadingState(description: "loading_history".localized()))
    }
    
    func presentFavoritesButton(selected: Bool) {
        let viewModel = CurrencyDetailViewModel.FavoritesButton(imageName: selected ? "like_selected" : "like")
        output.display(favoritesButton: viewModel)
    }
}
