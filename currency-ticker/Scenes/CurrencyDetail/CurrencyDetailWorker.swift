//
//  CurrencyDetailWorker.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 09.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Result

class CurrencyDetailWorker {
    private let lastMonthTimeInterval = TimeInterval(-60*60*24*30)
    
    func load(rateForCurrencyCode currencyCode: CurrencyCode, baseCurrencyCode: CurrencyCode, completion: @escaping (Result<CurrencyDetailResponse.CurrentRate, GetLatestRatesOperationError>) -> Void) {
        let operation = GetLatestRatesOperation(completion: { result in
            switch result {
            case .success(let currencies):
                let currencyResult = currencies.filter {$0.code == currencyCode }
                let baseCurrencyResult = currencies.filter {$0.code == baseCurrencyCode }
                if currencyResult.isEmpty == false && baseCurrencyResult.isEmpty == false {
                    completion(.success(CurrencyDetailResponse.CurrentRate(baseCurrency: baseCurrencyResult[0], currency: currencyResult[0])))
                } else {
                    completion(.failure(.generic))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        })
        NetworkQueue.shared.add(operation: operation)
    }
    
    func load(historyForCurrency currencyCode: CurrencyCode, baseCurrencyCode: CurrencyCode, completion: @escaping (Result<CurrencyDetailResponse.HistoricalRates, GetHistoricalRatesOperationError>) -> Void) {
        let operation = GetHistoricalRatesOperation(startDate: Date().addingTimeInterval(lastMonthTimeInterval), endDate: Date(), completion: { result in
            
            switch result {
            case .success(let rates):
                
                guard let baseRates = rates[baseCurrencyCode],
                    let currencyRates = rates[currencyCode],
                    baseRates.count == currencyRates.count
                    else { return completion(.failure(.generic)) }

                let response = CurrencyDetailResponse.HistoricalRates(baseCurrencies: baseRates.map { Currency(code: baseCurrencyCode, exchangeRate: $0)}, currencies: currencyRates.map { Currency(code: currencyCode, exchangeRate: $0)})
                    completion(.success(response))
            case .failure(let error):
                completion(.failure(error))
                
            }
        })
        NetworkQueue.shared.add(operation: operation)
    }
}
