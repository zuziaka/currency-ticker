//
//  CurrencyDetailInteractor.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 09.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Result

protocol CurrencyDetailInteractorInput {
    func loadInitialState()
    func loadChartData()
    func loadFavoritesButton()
    func didTapOnFavoritesButton()
    var currencyCode: CurrencyCode! { get set }
    var currency: Currency? { get set }
    var baseCurrency: Currency? { get set }
    var baseCurrencyCode: CurrencyCode! { get set }
}

protocol CurrencyDetailInteractorOutput {
    func present(currentRateResponse: Result<CurrencyDetailResponse.CurrentRate, GetLatestRatesOperationError>)
    func present(currency: Currency, baseCurrency: Currency)
    func present(historicalRatesResponse response: Result<CurrencyDetailResponse.HistoricalRates, GetHistoricalRatesOperationError>)
    func presentMainLoadingState()
    func presentChartLoadingState()
    func presentFavoritesButton(selected: Bool)
}

class CurrencyDetailInteractor: CurrencyDetailInteractorInput {
    var output: CurrencyDetailInteractorOutput!
    var worker: CurrencyDetailWorker = CurrencyDetailWorker()

    var currencyCode: CurrencyCode!
    var currency: Currency? {
        didSet {
            if let currency = currency { currencyCode = currency.code }
        }
    }
    
    var baseCurrency: Currency? {
        didSet {
            if let baseCurrency = baseCurrency { baseCurrencyCode = baseCurrency.code }
        }
    }
    
    var baseCurrencyCode: CurrencyCode!
    
    func loadInitialState() {
        if let currency = currency, let baseCurrency = baseCurrency {
            output.present(currency: currency, baseCurrency: baseCurrency)
        } else {
            loadCurrencyData()
        }
        
        loadChartData()
    }
    
    func loadCurrencyData() {
        output.presentMainLoadingState()
        worker.load(rateForCurrencyCode: currencyCode, baseCurrencyCode: baseCurrencyCode, completion: { [weak self] response in
            self?.output.present(currentRateResponse: response)
            })
    }
    
    func loadChartData() {
        output.presentChartLoadingState()
        worker.load(historyForCurrency: currencyCode, baseCurrencyCode: baseCurrencyCode, completion: { [weak self] response in
            self?.output.present(historicalRatesResponse: response)
            })
    }
    
    func didTapOnFavoritesButton() {
        if UserManager.isFavorite(currencyCode: currencyCode) {
            UserManager.remove(favoriteCurrencyCode: currencyCode)
        } else {
            UserManager.add(favoriteCurrencyCode: currencyCode)
        }
        loadFavoritesButton()
    }
    
    func loadFavoritesButton() {
        output.presentFavoritesButton(selected: UserManager.isFavorite(currencyCode: currencyCode))
    }
}
