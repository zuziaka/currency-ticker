//
//  CurrencyDetailViewController+Chart.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 11.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import SwiftCharts
import PureLayout

extension CurrencyDetailViewController {
    
    func setupChart(withData data: CurrencyDetailViewModel.CurrencyHistory) {
        loadChart(withData: data)
        return
    }
    
    func loadChart(withData data: CurrencyDetailViewModel.CurrencyHistory) {
        var i = 0.0
        let points: [(Double, Double)] = data.rates.map({ item in
            i = i.advanced(by: 1)
            return (i, item)
        })
        
        let chartPoints: [ChartPoint] = points.map{ChartPoint(x: ChartAxisValueDouble($0.0), y: ChartAxisValueDouble($0.1))}
        
        let labelSettings = ChartLabelSettings(font: userInterface.font.regular(withSize: 12.0))
        let dateLabelSettings = ChartLabelSettings(font: userInterface.font.regular(withSize: 12.0), rotation: 45, rotationKeep: .top, shiftXOnRotation: true, textAlignment: .default)
        guard let min = data.rates.min(), let max = data.rates.max() else {
            return
        }
        
        let y_min = min - 0.2*abs(max - min)
        let y_max = max + 0.2*abs(max - min)
        let y_step = 0.2*abs(max-min)
        
        let defaultFormatter = NumberFormatter()
        defaultFormatter.maximumFractionDigits = 3
        
        let xValues = stride(from: 1, through: data.labels.count-1, by: 3).map { ChartAxisValueString(data.labels[$0], order: Int($0), labelSettings: dateLabelSettings)}
        let yValues = stride(from: y_min, through: y_max, by: y_step).map {ChartAxisValueDouble($0, formatter: data.formatter ?? defaultFormatter, labelSettings: labelSettings)}
        
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "", settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "", settings: labelSettings.defaultVertical()))
        
        let chartFrame = ChartsManager.chartFrame(chartView.bounds)
        
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: ChartsManager.chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxis, yAxis, innerFrame) = (coordsSpace.xAxis, coordsSpace.yAxis, coordsSpace.chartInnerFrame)
        
        let lineModel = ChartLineModel(chartPoints: chartPoints, lineColor: userInterface.color.tint, lineWidth: 3.0, animDuration: 1, animDelay: 0)
        let chartPointsLineLayer = ChartPointsLineLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, lineModels: [lineModel])
        
        let chart = Chart(
            frame: chartFrame,
            layers: [
                coordsSpace.xAxis,
                coordsSpace.yAxis,
                chartPointsLineLayer
            ]
        )
        
        self.chartView.addSubview(chart.view)
        chart.view.autoPinEdgesToSuperviewEdges()
        
        self.chart = chart
    }

}
