//
//  CurrencyDetailConfigurator.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 09.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

// MARK: Connect View, Interactor, and Presenter

extension CurrencyDetailViewController: CurrencyDetailPresenterOutput { }

extension CurrencyDetailViewController: StoryboardInjectable {
    typealias T = CurrencyCode
    
    static func create(with currencyCode: CurrencyCode) -> CurrencyDetailViewController {
        let vc = create()
        vc.output.currencyCode = currencyCode
        vc.output.baseCurrencyCode = UserManager.baseCurrencyCode
        return vc
    }
    
    static func create(with currency: Currency, baseCurrency: Currency) -> CurrencyDetailViewController {
        let vc = create()
        vc.output.currency = currency
        vc.output.baseCurrency = baseCurrency
        return vc
    }
    
    private static func create() -> CurrencyDetailViewController {
        let bundle = Bundle(for: CurrencyDetailViewController.self)
        let storyboard = UIStoryboard(name: "CurrencyDetail", bundle: bundle)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CurrencyDetailViewController") as! CurrencyDetailViewController
        return viewController
    }
}

extension CurrencyDetailInteractor: CurrencyDetailViewControllerOutput { }

extension CurrencyDetailPresenter: CurrencyDetailInteractorOutput { }

class CurrencyDetailConfigurator {
    // MARK: Object lifecycle

    static let sharedInstance = CurrencyDetailConfigurator()
    private init() { }

    // MARK: Configuration

    func configure(viewController: CurrencyDetailViewController) {
        let router = CurrencyDetailRouter()
        router.viewController = viewController

        let presenter = CurrencyDetailPresenter()
        presenter.output = viewController

        let interactor = CurrencyDetailInteractor()
        interactor.output = presenter

        viewController.output = interactor
        viewController.router = router
        viewController.userInterface = ApplicationUserInterface()
    }
}
