//
//  CurrencyDetailModels.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 09.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import Result

struct CurrencyDetailResponse {
    struct CurrentRate {
        let baseCurrency: Currency
        let currency: Currency
    }
    
    struct HistoricalRates {
        let baseCurrencies: [Currency]
        let currencies: [Currency]
    }
}

struct CurrencyDetailViewModel {
    struct CurrencyInfo {
        let exchangeRate: String
        let base: String
        let title: String
        let description: String
        let date: String
        let source: String
    }
    
    struct CurrencyHistory {
        let rates: [Double]
        let formatter: NumberFormatter?
        let labels: [String]
    }
    struct Error {
        let description: String
        let actionTitle: String?
    }
    
    struct LoadingState {
        let description: String
    }
    
    struct FavoritesButton {
        let imageName: String
    }
}
