//
//  CurrencyDetailRouter.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 09.10.2016.
//  Copyright (c) 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import UIKit

protocol CurrencyDetailRouterInput { }

class CurrencyDetailRouter: CurrencyDetailRouterInput {
    weak var viewController: CurrencyDetailViewController!
}
