//
//  Double+Comparision.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

private func nearlyEqual(left: Double, right: Double, e: Double) -> Bool {
    if left == right { return true }
    let diff = abs(left - right)
    
    return diff < e
}

precedencegroup ComparisonPrecedence {
    associativity: left
    higherThan: LogicalConjunctionPrecedence
}

infix operator ~== : ComparisonPrecedence

public func ~==(left: Double, right: Double) -> Bool {
    return nearlyEqual(left: left, right: right, e: 0.001)
}
