//
//  UserBaseCurrency.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 09.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

public struct UserManager {
    private static let FavoritesKey = "FavoriteCurrencyCodes"
    private static let BaseCurrencyCodeKey = "BaseCurrencyCode"
    
    //MARK: Base currency code
    static var baseCurrencyCode: CurrencyCode {
        let defaults = UserDefaults.standard
        if let baseCode = defaults.object(forKey: BaseCurrencyCodeKey) as? String, let currencyCode = CurrencyCode(rawValue: baseCode) {
            return currencyCode
        }
        if let currencyCodeStringValue = Locale.current.currencyCode, let currency = CurrencyCode(rawValue: currencyCodeStringValue) {
            return currency
        } 
        
        return .EUR
    }
    
    static func add(baseCurrencyCode: CurrencyCode) {
        let defaults = UserDefaults.standard
        defaults.set(baseCurrencyCode.rawValue, forKey: BaseCurrencyCodeKey)
    }
    
    // MARK: Favorites
    static var favoriteCurrencyCodes: [CurrencyCode] {
        let defaults = UserDefaults.standard
        guard let favoritesArray = defaults.array(forKey: UserManager.FavoritesKey) as? [String] else {
            return []
        }
        return favoritesArray.flatMap { CurrencyCode(rawValue: $0) }
    }
    
    static func add(favoriteCurrencyCode code: CurrencyCode) {
        let codes = favoriteCurrencyCodes
        var setOfCodes = Set<String>(codes.map { $0.rawValue })
        setOfCodes.insert(code.rawValue)
        let defaults = UserDefaults.standard
        defaults.set(Array(setOfCodes), forKey: UserManager.FavoritesKey)
    }
    
    static func remove(favoriteCurrencyCode code: CurrencyCode) {
        var codes = Set<String>(favoriteCurrencyCodes.map { $0.rawValue })
        codes.remove(code.rawValue)
        let defaults = UserDefaults.standard
        defaults.set(Array(codes), forKey: UserManager.FavoritesKey)
    }
    
    static func removeAllFavorites() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: UserManager.FavoritesKey)
    }
    
    static func isFavorite(currencyCode: CurrencyCode) -> Bool {
        return favoriteCurrencyCodes.contains(currencyCode)
    }
}
