//
//  ExchangeRate.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

/// All exchange rate factors are based on PLN
public struct ExchangeRate {
    public let factor: Double
    public private(set) var date: Date = Date()
    
    init(factor: Double, date: Date = Date()) {
        self.factor = factor
        self.date = date
    }
}

extension ExchangeRate: ExpressibleByFloatLiteral, ExpressibleByIntegerLiteral {
    public init(floatLiteral value: Double) { self.factor = value }
    public init(integerLiteral value: Int) { self.factor = Double(value) }
}
