//
//  Currency.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation

public enum CurrencyCode: String {
    case THB = "THB"
    case USD = "USD"
    case AUD = "AUD"
    case HKD = "HKD"
    case CAD = "CAD"
    case NZD = "NZD"
    case SGD = "SGD"
    case EUR = "EUR"
    case HUF = "HUF"
    case CHF = "CHF"
    case GBP = "GBP"
    case UAH = "UAH"
    case JPY = "JPY"
    case CZK = "CZK"
    case DKK = "DKK"
    case ISK = "ISK"
    case NOK = "NOK"
    case SEK = "SEK"
    case HRK = "HRK"
    case RON = "RON"
    case BGN = "BGN"
    case TRY = "TRY"
    case ILS = "ILS"
    case CLP = "CLP"
    case PHP = "PHP"
    case MXN = "MXN"
    case ZAR = "ZAR"
    case BRL = "BRL"
    case MYR = "MYR"
    case RUB = "RUB"
    case IDR = "IDR"
    case INR = "INR"
    case KRW = "KRW"
    case CNY = "CNY"
    case XDR = "XDR"
    case PLN = "PLN"
    
    static var allValues: [CurrencyCode] {
        return [.THB,.USD,.AUD,.HKD,.CAD,.NZD,.SGD,.EUR,.HUF,.CHF,.GBP,.UAH,.JPY,.CZK,.DKK,.ISK,.NOK,.SEK,.HRK,.RON,.BGN,.TRY,.ILS,.CLP,.PHP,.MXN,.ZAR,.BRL,.MYR,.RUB,.IDR,.INR,.KRW,.CNY,.XDR,.PLN]
    }
}


public struct Currency {
    let code: CurrencyCode
    let exchangeRate: ExchangeRate
    var factor: Double { return exchangeRate.factor }
}

extension Currency {
    static func base() -> Currency {
        return Currency(code: .PLN, exchangeRate: 1.0)
    }
    
    var name: String {
        return Locale.current.localizedString(forCurrencyCode: code.rawValue)!
    }
}
