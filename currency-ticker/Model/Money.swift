//
//  Money.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 06.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation


public struct Money {
    let currency: Currency
    var amount: Double
    
    init(currency: Currency, amount: Double) {
        self.currency = currency
        self.amount = amount
    }
    
    public func convert(to otherCurrency: Currency) -> Money {
        let baseAmount = amount * currency.factor
        let convertedAmount = baseAmount / otherCurrency.factor
        return Money(currency: otherCurrency, amount: convertedAmount)
    }
}

extension Money: CustomStringConvertible {
    public var description: String {
        return numberFormatter.string(from: NSNumber(value: self.amount))!
    }
    
    public var numberFormatter: NumberFormatter {
        let f = NumberFormatter()
        f.numberStyle = .currency
        f.maximumFractionDigits = 4
        f.currencyCode = currency.code.rawValue
        return f
    }
}

extension Money: Equatable { }

public func ==(lhs: Money, rhs: Money) -> Bool {
    return lhs.convert(to: rhs.currency).amount ~== rhs.amount
}
