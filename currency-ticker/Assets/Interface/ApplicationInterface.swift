//
//  ApplicationFont.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 05.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import UIKit

// MARK: Base Protocol

public protocol UserInterface {
    var font: Font { get }
    var color: Colors { get }
}

public protocol Font {
    func regular(withSize: Double) -> UIFont
    func bold(withSize: Double) -> UIFont
}

public protocol Colors {
    var tint: UIColor { get }
    var tint2: UIColor { get }
    var plain: UIColor { get }
    var background: UIColor { get }
    var light: UIColor { get }
}

public protocol Layers {
    func gradient(withFrame: CGRect) -> CAGradientLayer
}

// MARK: Application Values

public struct ApplicationUserInterface: UserInterface {
    public var font: Font = ArialFont()
    public var color: Colors = ApplicationColors()
}

public struct ApplicationColors: Colors {
    public var tint: UIColor = #colorLiteral(red: 0.1291020215, green: 0.7930518985, blue: 0.6780599952, alpha: 1)
    public var tint2: UIColor = #colorLiteral(red: 0.2469461858, green: 0.8260261416, blue: 0.4993686676, alpha: 1)
    public var plain: UIColor = #colorLiteral(red: 0.3568627451, green: 0.3568627451, blue: 0.3568627451, alpha: 1)
    public var background: UIColor = #colorLiteral(red: 0.9489166141, green: 0.9490789771, blue: 0.9489062428, alpha: 1)
    public var light: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
}

public struct ArialFont: Font {
    public func regular(withSize size: Double) -> UIFont {
        return UIFont(name: "ArialRoundedMT", size: CGFloat(size))!
    }
    
    public func bold(withSize size: Double) -> UIFont {
        return UIFont(name: "ArialRoundedMTBold", size: CGFloat(size))!
    }
}
