//
//  ApplicationInterface+Gradient.swift
//  currency-ticker
//
//  Created by Axel Zuziak on 11.10.2016.
//  Copyright © 2016 SwiftyDev Axel Zuziak. All rights reserved.
//

import Foundation
import UIKit

extension UserInterface {
    func gradientLayer(withColors colors: [UIColor], frame: CGRect? = nil) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        if let frame = frame {
            gradientLayer.frame = frame
        }
        
        gradientLayer.colors = colors.map { $0.cgColor }
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        
        gradientLayer.locations = colors.map { NSNumber(value: Double(colors.index(of: $0)! + 1) / Double(colors.count))}
        return gradientLayer
        
    }
}
